#include "Ellipse.h"
#include <SDL.h>
#include <iostream>

using namespace std;

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	
	float ab, bb, p;
	int x, y;
	ab = a*a;
	bb = b*b;
	p = 2 * ((float)bb / ab) - (2 * b) + 1;
	x = 0;
	y = b;
	// Area 1
	while ( ((float)bb / ab) *x <= y)
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p >= 0)
		{
			p = p - 4 * y + 2 * ((float)bb / ab)*(2 * x + 3);
			y--;
		}
		else
			p = p + 2 * ((float)bb / ab)*(2 * x + 3);
		x++;
		
	}

    // Area 2
	x = a;
	y = 0;
	p = 1 + 2 * ((float)ab / bb) - 2*a;
	while ( ((float)ab/bb)*y <= x )
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p >=0)
		{
			p = p - 4 * x + 2 * ((float)ab/bb)*(2 * y + 3);
			x --;
		}
		else
			p = p + 2 * ((float)ab/bb)*(2 * y + 3);
		y ++;
		}
}	

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    // Area 1
	int x, y;
	float p;
	x = 0;
	y = b;
	Draw4Points(xc, yc, x, y, ren);
	p = b * b - a * a*b + (1 / 4)*a*a;
	while (((b*b) / (a*a))*(x) < y)
	{
		x++;
		if (p < 0)
			p = p + b * b + 2 * b*b*x;
		else
		{
			y--;
			p = p + b * b + 2 * x*b*b - 2 * a*a*y;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
		// Area 2
	p = b * b *(x + 1 / 2)*(x + 1 / 2) + a * a*(y - 1)*(y - 1) - a * a*b*b;
		while (y>0)
	{
		y--;
		if (p > 0)
			p = p + a * a - 2 * a*a*y;
		else
		{
			x++;
			p = p + a * a + 2 * x*b*b - 2 * a*a*y;
		}
		Draw4Points(xc, yc, x, y, ren);
	}
}