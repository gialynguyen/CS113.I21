#include "stdafx.h"
#include <iostream>
#include <SDL.h>
#include "Bezier.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;
SDL_bool done = SDL_FALSE;
SDL_Event event;
int i = 0;
int flagDrawn = 0;
int mousePosX, mousePosY;
int xnew, ynew;
int x[4], y[4];
int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);


	SDL_Rect *rect1 = new SDL_Rect();
	rect1->x = p1.x - 3;
	rect1->y = p1.y - 3;
	rect1->w = 6;
	rect1->h = 6;

	SDL_Rect *rect2 = new SDL_Rect();
	rect2->x = p2.x - 3;
	rect2->y = p2.y - 3;
	rect2->w = 6;
	rect2->h = 6;

	SDL_Rect *rect3 = new SDL_Rect();
	rect3->x = p3.x - 3;
	rect3->y = p3.y - 3;
	rect3->w = 6;
	rect3->h = 6;

	SDL_Rect *rect4 = new SDL_Rect();
	rect4->x = p4.x - 3;
	rect4->y = p4.y - 3;
	rect4->w = 6;
	rect4->h = 6;

	SDL_Color colorCurve = { 100, 20, 40, 255 }, colorRect = { 0, 255, 40, 255 };
	x[0] = p1.x; y[0] = p1.y;
	x[1] = p2.x; y[1] = p2.y;
	x[2] = p3.x; y[2] = p3.y;
	x[3] = p4.x; y[3] = p4.y;
	
	while (!done)
	{
		if (i == 4)
		{
			SDL_SetRenderDrawColor(ren, colorCurve.r, colorCurve.g, colorCurve.b, colorCurve.a);
			DrawCurve3(ren, x,y);
			flagDrawn = 1;
		}
		SDL_SetRenderDrawColor(ren, 128, 128, 128, SDL_ALPHA_OPAQUE);
		circleBres(x[0], y[0], 8,ren);


		SDL_SetRenderDrawColor(ren, 255, 0, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderDrawLine(ren, x[0], y[0], x[1], y[1]);


		SDL_SetRenderDrawColor(ren, 128, 128, 128, SDL_ALPHA_OPAQUE);
		circleBres(x[1], y[1], 8,ren);


		SDL_SetRenderDrawColor(ren, 255, 0, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderDrawLine(ren, x[1], y[1], x[2], y[2]);


		SDL_SetRenderDrawColor(ren, 128, 128, 128, SDL_ALPHA_OPAQUE);
		circleBres(x[2], y[2], 8,ren);


		SDL_SetRenderDrawColor(ren, 255, 0, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderDrawLine(ren, x[2], y[2], x[3], y[3]);

		/*grey color circle to encircle control Point P3*/
		SDL_SetRenderDrawColor(ren, 128, 128, 128, SDL_ALPHA_OPAQUE);
		circleBres(x[3], y[3], 8,ren);

		//Take a quick break after all that hard work
		//Quit if happen QUIT event
		bool running = true;

		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				done = SDL_TRUE;
			}
			/*Mouse Button is Down */
			if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				/*If left mouse button down then store
				that point as control point*/
				if (event.button.button == SDL_BUTTON_LEFT)
				{
					/*store only four points
					because of cubic bezier curve*/
					if (i < 4)
					{
						/*Storing Mouse x and y positions
						in our x and y coordinate array */
						x[i] = mousePosX;
						y[i] = mousePosY;
						i++;
					}
				}
			}
			/*Mouse is in motion*/
			if (event.type == SDL_MOUSEMOTION)
			{
				/*get x and y postions from motion of mouse*/
				xnew = event.motion.x;
				ynew = event.motion.y;

				int j;

				/* change coordinates of control point
				after bezier curve has been drawn */
				if (flagDrawn == 1)
				{
					for (j = 0; j < i; j++)
					{
						/*Check mouse position if in b/w circle then
						change position of that control point to mouse new
						position which are coming from mouse motion*/
						if ((float)sqrt(abs(xnew - x[j]) * abs(xnew - x[j])
							+ abs(ynew - y[j]) * abs(ynew - y[j])) < 8.0)
						{
							/*change coordinate of jth control point*/
							x[j] = xnew;
							y[j] = ynew;
						}
					}
				}
				/*updating mouse positions to positions
				coming from motion*/
				mousePosX = xnew;
				mousePosY = ynew;
			}
		}
	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
