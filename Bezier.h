#ifndef GRAPHICS2D_BEZIER_H
#define GRAPHICS2D_BEZIER_H

#include <SDL.h>
#include "Vector2D.h"
#include <vector>
void drawCircle(int xc, int yc, int x, int y, SDL_Renderer *renderer);
void circleBres(int xc, int yc, int r, SDL_Renderer *ren);
void DrawCurve2(SDL_Renderer *ren, int x[],int y[]);
void DrawCurve3(SDL_Renderer *ren, int x[], int y[])

#endif //GRAPHICS2D_BEZIER_H
