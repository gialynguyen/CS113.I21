#include "Bezier.h"
#include <iostream>
using namespace std;
void drawCircle(int xc, int yc, int x, int y,SDL_Renderer *renderer)
{
	SDL_RenderDrawPoint(renderer, xc + x, yc + y);
	SDL_RenderDrawPoint(renderer, xc - x, yc + y);
	SDL_RenderDrawPoint(renderer, xc + x, yc - y);
	SDL_RenderDrawPoint(renderer, xc - x, yc - y);
	SDL_RenderDrawPoint(renderer, xc + y, yc + x);
	SDL_RenderDrawPoint(renderer, xc - y, yc + x);
	SDL_RenderDrawPoint(renderer, xc + y, yc - x);
	SDL_RenderDrawPoint(renderer, xc - y, yc - x);
}
void circleBres(int xc, int yc, int r,SDL_Renderer *ren)
{
	int x = 0, y = r;
	int d = 3 - 2 * r;
	while (y >= x)
	{
		/*for each pixel we will draw all eight pixels */
		drawCircle(xc, yc, x, y,ren);
		x++;

		/*check for decision parameter and correspondingly update d, x, y*/
		if (d > 0)
		{
			y--;
			d = d + 4 * (x - y) + 10;
		}
		else
			d = d + 4 * x + 6;
		drawCircle(xc, yc, x, y,ren);
	}
}
void DrawCurve2(SDL_Renderer *ren,int x[],int y[])
{
	double xu = 0.0, yu = 0.0, u = 0.0;
	int i = 0;
	for (u = 0.0; u <= 1.0; u += 0.0001)
	{
		xu = pow(1 - u, 3)*x[0] + 3 * u*pow(1 - u, 2)*x[1] + 3 * pow(u, 2)*(1 - u)*x[2]
			+ pow(u, 3)*x[3];
		yu = pow(1 - u, 3)*y[0] + 3 * u*pow(1 - u, 2)*y[1] + 3 * pow(u, 2)*(1 - u)*y[2]
			+ pow(u, 3)*y[3];
		SDL_RenderDrawPoint(ren, (int)xu, (int)yu);
	}
}
void DrawCurve3(SDL_Renderer *ren, int x[], int y[])
{
	double xu = 0.0, yu = 0.0, u = 0.0;
	int i = 0;
	for (u = 0.0; u <= 1.0; u += 0.0001)
	{
		xu = pow(1 - u, 2)*x[0] + 2 * u*pow(1 - u, 1)*x[1] + u*u*x[2];
		yu = pow(1 - u, 2)*y[0] + 2 * u*pow(1 - u, 1)*y[1] + u*u*y[2];
		SDL_RenderDrawPoint(ren, (int)xu, (int)yu);
	}
}
